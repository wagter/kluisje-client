<?php

namespace Wagter\KluisjeClient\Normalizer;

use Wagter\KluisjeClient\Model\LinkStatus;
use Wagter\KluisjeClient\Model\LinkStatusList;

/**
 * To implement a normalizer for LinkStatus objects
 *
 * Interface LinkStatusNormalizerInterface
 * @package Wagter\KluisjeClient\Normalizer
 *
 * @author Joris Wagter
 */
interface LinkStatusNormalizerInterface
{
    /**
     * Normalize a LinkStatus object to a JSON object
     *
     * @param LinkStatus $linkStatus
     *
     * @return string
     */
    public function normalizeToJson( LinkStatus $linkStatus ): string;
    
    /**
     * Normalize a LinkStatus object to an associative array
     *
     * @param LinkStatus $linkStatus
     *
     * @return array
     */
    public function normalizeToArray( LinkStatus $linkStatus ): array;
    
    /**
     * Normalize a LinkStatusList object to a JSON object
     *
     * @param LinkStatusList $linkStatusList
     *
     * @return string
     */
    public function normalizeListToJson( LinkStatusList $linkStatusList ): string;
    
    /**
     * Normalize a LinkStatusList object to an associative array
     *
     * @param LinkStatusList $linkStatusList
     *
     * @return array
     */
    public function normalizeListToArray( LinkStatusList $linkStatusList ): array;
}