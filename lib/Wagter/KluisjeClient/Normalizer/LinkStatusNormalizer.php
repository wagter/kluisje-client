<?php

namespace Wagter\KluisjeClient\Normalizer;

use Wagter\KluisjeClient\Model\LinkStatus;
use Wagter\KluisjeClient\Model\LinkStatusList;

/**
 * Use to normalize LinkStatus objects for web requests
 *
 * Class LinkStatusNormalizer
 * @package Wagter\KluisjeClient\Normalizer
 *
 * @author Joris Wagter
 */
class LinkStatusNormalizer implements LinkStatusNormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalizeToJson( LinkStatus $linkStatus ): string
    {
        return json_encode( $this->normalizeToArray( $linkStatus ) );
    }
    
    /**
     * {@inheritdoc}
     */
    public function normalizeToArray( LinkStatus $linkStatus ): array
    {
        return [
            'fileName'        => $linkStatus->getFileName(),
            'hash'            => $linkStatus->getHash(),
            'validUntil'      => $linkStatus->getValidUntil() !== null ? $linkStatus->getValidUntil()->format( 'Y-m-d H:i:sP' ) : null,
            'downloadLimit'   => $linkStatus->getDownloadLimit(),
            'timesDownloaded' => $linkStatus->getTimesDownloaded(),
            'status'          => $linkStatus->getStatus(),
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function normalizeListToJson( LinkStatusList $linkStatusList ): string
    {
        return (string)json_encode( $this->normalizeListToArray( $linkStatusList ) );
    }
    
    /**
     * {@inheritdoc}
     */
    public function normalizeListToArray( LinkStatusList $linkStatusList ): array
    {
        $list = [];
        
        foreach ( $linkStatusList as $status ) {
            $list[] = $this->normalizeToArray( $status );
        }
        
        return [ 'linkStatusList' => $list ];
    }
}