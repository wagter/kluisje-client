<?php

namespace Wagter\KluisjeClient\Normalizer;

use Wagter\KluisjeClient\Model\FileStatus;
use Wagter\KluisjeClient\Model\FileStatusList;

/**
 * To implement a normalizer for FileStatus objects
 *
 * Interface FileStatusNormalizerInterface
 * @package Wagter\KluisjeClient\Normalizer
 *
 * @author Joris Wagter
 */
interface FileStatusNormalizerInterface
{
    /**
     * Normalize a FileStatus object to a JSON object
     *
     * @param FileStatus $fileStatus
     *
     * @return string
     */
    public function normalizeToJson( FileStatus $fileStatus ): string;
    
    /**
     * Normalize a FileStatus object to an associative array
     *
     * @param FileStatus $fileStatus
     *
     * @return array
     */
    public function normalizeToArray( FileStatus $fileStatus ): array;
    
    /**
     * Normalize a FileStatusList object to a JSON object
     *
     * @param FileStatusList $fileStatusList
     *
     * @return string
     */
    public function  normalizeListToJson( FileStatusList $fileStatusList ): string;
    
    /**
     * Normalize a FileStatusList object to an associative array
     *
     * @param FileStatusList $fileStatusList
     *
     * @return array
     */
    public function normalizeListToArray( FileStatusList $fileStatusList ): array;
}