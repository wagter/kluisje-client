<?php

namespace Wagter\KluisjeClient\Normalizer;

use Wagter\KluisjeClient\Model\FileStatus;
use Wagter\KluisjeClient\Model\FileStatusList;

/**
 * Use to normalize FileStatus objects for web requests
 *
 * Class FileStatusNormalizer
 * @package Wagter\KluisjeClient\Normalizer
 *
 * @author Joris Wagter
 */
class FileStatusNormalizer implements FileStatusNormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalizeToJson( FileStatus $fileStatus ): string
    {
        return (string)json_encode( $this->normalizeToArray( $fileStatus ) );
    }
    
    /**
     * {@inheritdoc}
     */
    public function normalizeToArray( FileStatus $fileStatus ): array
    {
        return [
            'fileName'    => $fileStatus->getFileName(),
            'fileSize'    => $fileStatus->getFileSize(),
            'fileType'    => $fileStatus->getFileType(),
            'fileMTime'   => $fileStatus->getFileMTime(),
            'fileExists'  => $fileStatus->fileExists(),
            'fileDeleted' => $fileStatus->isFileDeleted(),
            'error'       => $fileStatus->getError(),
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function normalizeListToJson( FileStatusList $fileStatusList ): string
    {
        return (string)json_encode( $this->normalizeListToArray( $fileStatusList ) );
    }
    
    /**
     * {@inheritdoc}
     */
    public function normalizeListToArray( FileStatusList $fileStatusList ): array
    {
        $list = [];
        
        foreach ( $fileStatusList as $status ) {
            $list[] = $this->normalizeToArray( $status );
        }
        
        return [ 'fileStatusList' => $list ];
    }
}