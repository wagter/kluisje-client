<?php

namespace Wagter\KluisjeClient\Normalizer\Denormalizer;

use Wagter\KluisjeClient\Model\LinkStatus;
use Wagter\KluisjeClient\Model\LinkStatusList;

/**
 * Interface to implement a de-normalizer for LinkStatus objects
 *
 * Interface LinkStatusDenormalizerInterface
 * @package Wagter\KluisjeClient\Normalizer\Denormalizer
 *
 * @author Joris Wagter
 */
interface LinkStatusDenormalizerInterface
{
	/**
	 * Try to denormalize a JSON object to a LinkStatus object
	 *
	 * @param string $json
	 *
	 * @return LinkStatus|null
	 */
	public function denormalizeFromJson( string $json ): LinkStatus;
    
    /**
     * Try to denormalize am associative array to a LinkStatus object
     *
     * @param array $array
     *
     * @return LinkStatus
     */
	public function denormalizeFromArray( array $array ): LinkStatus;
    
    /**
     * Try to denormalize a JSON object to a LinkStatusList object
     *
     * @param string $json
     *
     * @return LinkStatusList
     */
	public function denormalizeListFromJson( string $json ): LinkStatusList;
    
    /**
     * Try to denormalize am associative array to a LinkStatusList object
     *
     * @param array $array
     *
     * @return LinkStatusList
     */
	public function denormalizeListFromArray( array $array ): LinkStatusList;
}