<?php

namespace Wagter\KluisjeClient\Normalizer\Denormalizer;

use Wagter\KluisjeClient\Model\FileStatus;
use Wagter\KluisjeClient\Model\FileStatusList;

/**
 * Use to denormalize FileStatus objects from array or JSON
 *
 * Class FileStatusDenormalizer
 * @package Wagter\KluisjeClient\Normalizer\Denormalizer
 *
 * @author Joris Wagter
 */
class FileStatusDenormalizer implements FileStatusDenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function denormalizeFromJson( string $json ): FileStatus
    {
        $data = json_decode( $json, true );
        
        if ( !is_array( $data ) ) {
            return new FileStatus();
        }
        
        return $this->denormalizeFromArray( $data );
    }
    
    /**
     * {@inheritdoc}
     */
    public function denormalizeFromArray( array $array ): FileStatus
    {
        $fileStatus = new FileStatus();
        
        return
            $fileStatus
                ->setFileName( @$array['fileName'] )
                ->setFileSize( @$array['fileSize'] )
                ->setFileType( @$array['fileType'] )
                ->setFileMTime( @$array['fileMTime'] )
                ->setFileExists( (bool)@$array['fileExists'] )
                ->setFileDeleted( (bool)@$array['fileDeleted'] )
                ->setError( @$array['error'] )
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function denormalizeListFromJson( string $json ): FileStatusList
    {
        $data = json_decode( $json, true );
        
        if ( !is_array( $data ) ) {
            return new FileStatusList();
        }
        
        return $this->denormalizeListFromArray( $data );
    }
    
    /**
     * {@inheritdoc}
     */
    public function denormalizeListFromArray( array $array ): FileStatusList
    {
        $list = new FileStatusList();
        
        if ( !array_key_exists( 'fileStatusList', $array ) || !is_array( $array['fileStatusList'] ) ) {
            return $list;
        }
        
        foreach ( $array['fileStatusList'] as $status ) {
            $list->add( $this->denormalizeFromArray( $status ) );
        }
        
        return $list;
    }
}