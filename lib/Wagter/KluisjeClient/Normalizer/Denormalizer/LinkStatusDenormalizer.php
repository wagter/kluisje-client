<?php

namespace Wagter\KluisjeClient\Normalizer\Denormalizer;

use Wagter\KluisjeClient\Model\LinkStatus;
use Wagter\KluisjeClient\Model\LinkStatusList;

/**
 * Use to denormalize LinkStatus object from array or JSON
 *
 * Class LinkStatusDenormalizer
 * @package Wagter\KluisjeClient\Normalizer\Denormalizer
 *
 * @author Joris Wagter
 */
class LinkStatusDenormalizer implements LinkStatusDenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function denormalizeFromJson( string $json ): LinkStatus
    {
        $data = json_decode( $json, true );
        
        if ( !is_array( $data ) ) {
            return new LinkStatus();
        }
        
        return $this->denormalizeFromArray( $data );
    }
    
    /**
     * {@inheritdoc}
     */
    public function denormalizeFromArray( array $array ): LinkStatus
    {
        $status = new LinkStatus();
        
        return
            $status
                ->setFileName( @$array['fileName'] )
                ->setHash( @$array['hash'] )
                ->setValidUntil( array_key_exists( 'validUntil', $array ) ? new \DateTime( $array['validUntil'] ) : null )
                ->setDownloadLimit( (int)@$array['downloadLimit'] )
                ->setTimesDownloaded( (int)@$array['timesDownloaded'] )
                ->setStatus( @$array['status'] )
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function denormalizeListFromJson( string $json ): LinkStatusList
    {
        $data = json_decode( $json, true );
        
        if ( !is_array( $data ) ) {
            return new LinkStatusList();
        }
        
        return $this->denormalizeListFromArray( $data );
    }
    
    /**
     * {@inheritdoc}
     */
    public function denormalizeListFromArray( array $array ): LinkStatusList
    {
        $list = new LinkStatusList();
        
        if ( !array_key_exists( 'linkStatusList', $array ) || !is_array( $array['linkStatusList'] ) ) {
            return $list;
        }
        
        foreach ( $array['linkStatusList'] as $status ) {
            $list->add( $this->denormalizeFromArray( $status ) );
        }
        
        return $list;
    }
}