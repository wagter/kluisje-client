<?php

namespace Wagter\KluisjeClient\Normalizer\Denormalizer;

use Wagter\KluisjeClient\Model\FileStatus;
use Wagter\KluisjeClient\Model\FileStatusList;

/**
 * Interface to implement a de-normalizer for FileStatus objects
 *
 * Interface FileStatusDenormalizerInterface
 * @package Wagter\KluisjeClient\Normalizer\Denormalizer
 *
 * @author Joris Wagter
 */
interface FileStatusDenormalizerInterface
{
    /**
     * Try to denormalize a JSON object to a FileStatus object
     *
     * @param string $json
     *
     * @return FileStatus
     */
    public function denormalizeFromJson( string $json ): FileStatus;
    
    /**
     * Try to denormalize an associative array to a FileStatus object
     *
     * @param array $array
     *
     * @return FileStatus
     */
    public function denormalizeFromArray( array $array ): FileStatus;
    
    /**
     * Try to denormalize a JSON object to a FileStatusList object
     *
     * @param string $json
     *
     * @return FileStatusList
     */
    public function denormalizeListFromJson( string $json ): FileStatusList;
    
    /**
     * Try to denormalize an associative array to a FileStatusList object
     *
     * @param array $array
     *
     * @return FileStatusList
     */
    public function denormalizeListFromArray( array $array ): FileStatusList;
}