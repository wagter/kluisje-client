<?php

namespace Wagter\KluisjeClient\Model;

/**
 * A model containing relevant information about a file on the server
 *
 * Class FileStatus
 * @package Wagter\KluisjeClient\Model
 *
 * @author Joris Wagter
 */
class FileStatus
{
    /**
     * The name of the file
     *
     * @var null|string
     */
    private $fileName = null;
    
    /**
     * The size of the file in bytes
     *
     * @var null|int
     */
    private $fileSize = null;
    
    /**
     * The mime-type of the file
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
     *
     * @var null|string
     */
    private $fileType = null;
    
    /**
     * The unix timestamp of when the file was last modified
     *
     * @var null|int
     */
    private $fileMTime = null;
    
    /**
     * If the file exists
     *
     * @var bool
     */
    private $fileExists = false;
    
    /**
     * If the file is deleted
     *
     * @var bool
     */
    private $fileDeleted = false;
    
    /**
     * The error message
     *
     * @var null|string
     */
    private $error = null;
    
    /**
     * Get the the name of the file
     *
     * @return null|string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }
    
    /**
     * Set the the name of the file
     *
     * @param null|string $fileName
     *
     * @return FileStatus
     */
    public function setFileName( ?string $fileName = null ): FileStatus
    {
        $this->fileName = $fileName;
        
        return $this;
    }
    
    /**
     * Get the size of the file in bytes
     *
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }
    
    /**
     * Set the size of the file in bytes
     *
     * @param int|null $fileSize
     *
     * @return FileStatus
     */
    public function setFileSize( ?int $fileSize = null ): FileStatus
    {
        $this->fileSize = $fileSize;
        
        return $this;
    }
    
    /**
     * Get the mime-type of the file
     *
     * @return null|string
     */
    public function getFileType(): ?string
    {
        return $this->fileType;
    }
    
    /**
     * Set the mime-type of the file
     *
     * @param null|string $fileType
     *
     * @return FileStatus
     */
    public function setFileType( ?string $fileType = null ): FileStatus
    {
        $this->fileType = $fileType;
        
        return $this;
    }
    
    /**
     * Get the unix timestamp of when the file was last modified
     *
     * @return int|null
     */
    public function getFileMTime(): ?int
    {
        return $this->fileMTime;
    }
    
    /**
     * Set the unix timestamp of when the file was last modified
     *
     * @param int|null $fileMTime
     *
     * @return FileStatus
     */
    public function setFileMTime( ?int $fileMTime = null ): FileStatus
    {
        $this->fileMTime = $fileMTime;
        
        return $this;
    }
    
    /**
     * Check if the file exists
     *
     * @return bool
     */
    public function fileExists(): bool
    {
        return $this->fileExists;
    }
    
    /**
     * Set if the file exists
     *
     * @param bool $fileExists
     *
     * @return FileStatus
     */
    public function setFileExists( bool $fileExists = false ): FileStatus
    {
        $this->fileExists = $fileExists;
        
        return $this;
    }
    
    /**
     * Check if the file is deleted
     *
     * @return bool
     */
    public function isFileDeleted(): bool
    {
        return $this->fileDeleted;
    }
    
    /**
     * Set if the file is deleted
     *
     * @param bool $fileDeleted
     *
     * @return FileStatus
     */
    public function setFileDeleted( bool $fileDeleted ): FileStatus
    {
        $this->fileDeleted = $fileDeleted;
        
        return $this;
    }
    
    /**
     * Get the error message
     *
     * @return null|string
     */
    public function getError(): ?string
    {
        return $this->error;
    }
    
    /**
     * Set the error message
     *
     * @param null|string $error
     *
     * @return FileStatus
     */
    public function setError( ?string $error = null ): FileStatus
    {
        $this->error = $error;
        
        return $this;
    }
}