<?php

namespace Wagter\KluisjeClient\Model;

/**
 * A model containing all relevant information about a download link requested from a Kluisje API installation.
 *
 * Class LinkResponse
 * @package Wagter\KluisjeClient\Model
 *
 * @author Joris Wagter
 */
class LinkStatus
{
    const STATUS_VALID = 'link_valid';
    const STATUS_EXPIRED = 'link_expired';
    const STATUS_LIMIT_REACHED = 'link_download_limit_reached';
    const STATUS_DELETED = 'link_deleted';
    const STATUS_NOT_EXISTS = 'link_not_exists';
    const STATUS_FILE_NOT_EXISTS = 'file_not_exists';
    
    /**
     * The file name of the requested download link
     *
     * @var null|string
     */
    private $fileName;
    
    /**
     * The hash of the requested download link
     *
     * @var null|string
     */
    private $hash;
    
    /**
     * The date/time until when the requested download link is valid
     *
     * @var null|\DateTimeInterface
     */
    private $validUntil = null;
    
    /**
     * The total times the requested download link may be downloaded
     *
     * @var int
     */
    private $downloadLimit = 0;
    
    /**
     * The times the requested download link is downloaded yet
     *
     * @var int
     */
    private $timesDownloaded = 0;
    
    /**
     * The status of the download link. See constants with STATUS_ prefix
     *
     * @var null|string
     */
    private $status = null;
    
    /**
     * LinkResponse constructor.
     *
     * @param string $fileName
     * @param null|string $hash
     */
    public function __construct(
        string $fileName = null,
        string $hash = null
    ) {
        $this->fileName = $fileName;
        $this->hash     = $hash;
    }
    
    /**
     * Set the file name of the requested download link
     *
     * @return string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }
    
    /**
     * Get the file name of the requested download link
     *
     * @param string $fileName
     *
     * @return LinkStatus
     */
    public function setFileName( string $fileName = null ): LinkStatus
    {
        $this->fileName = $fileName;
        
        return $this;
    }
    
    /**
     * Get the hash of the requested download link
     *
     * @return null|string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }
    
    /**
     * Set the hash of the requested download link
     *
     * @param null|string $hash
     *
     * @return LinkStatus
     */
    public function setHash( string $hash = null ): LinkStatus
    {
        $this->hash = $hash;
        
        return $this;
    }
    
    /**
     * Get the date/time until when the requested download link is valid
     *
     * @return \DateTimeInterface|null
     */
    public function getValidUntil(): ?\DateTimeInterface
    {
        return $this->validUntil;
    }
    
    /**
     * Set the date/time until when the requested download link is valid
     *
     * @param \DateTimeInterface $validUntil
     *
     * @return LinkStatus
     */
    public function setValidUntil( \DateTimeInterface $validUntil = null ): LinkStatus
    {
        $this->validUntil = $validUntil;
        
        return $this;
    }
    
    /**
     * Get the total times the requested download link may be downloaded
     *
     * @return int
     */
    public function getDownloadLimit(): int
    {
        return $this->downloadLimit;
    }
    
    /**
     * Set the total times the requested download link may be downloaded
     *
     * @param int $downloadLimit
     *
     * @return LinkStatus
     */
    public function setDownloadLimit( int $downloadLimit = null ): LinkStatus
    {
        $this->downloadLimit = $downloadLimit;
        
        return $this;
    }
    
    /**
     * Get the times the requested download link is downloaded yet
     *
     * @return int
     */
    public function getTimesDownloaded(): int
    {
        return $this->timesDownloaded;
    }
    
    /**
     * Set the times the requested download link is downloaded yet
     *
     * @param int $timesDownloaded
     *
     * @return LinkStatus
     */
    public function setTimesDownloaded( int $timesDownloaded = null ): LinkStatus
    {
        $this->timesDownloaded = $timesDownloaded;
        
        return $this;
    }
    
    /**
     * Get the status of the requested download link
     *
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    
    /**
     * Set the status of the requested download link
     *
     * @param null|string $status
     *
     * @return LinkStatus
     */
    public function setStatus( ?string $status ): LinkStatus
    {
        $this->status = $status;
        
        return $this;
    }
}