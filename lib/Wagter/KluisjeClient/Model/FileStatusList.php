<?php

namespace Wagter\KluisjeClient\Model;

/**
 * Class FileStatusList
 *
 * @package Wagter\KluisjeClient\Model
 *
 * @author Joris Wagter
 */
class FileStatusList implements \Countable, \Iterator
{
	/**
	 * The array containing the FileStatus objects.
	 *
	 * @var FileStatus[]
	 */
	private $fileStatuses = [];
	
	/**
	 * The current index of the iterator
	 *
	 * @var int
	 */
	private $current = 0;
	
	/**
	 * FileStatusList constructor.
	 *
	 * @param FileStatus[] $fileStatuses
	 */
	public function __construct( array $fileStatuses = [] )
	{
		foreach ( $fileStatuses as $fileStatus ) {
			$this->add( $fileStatus );
		}
	}
	
	/**
	 * Add a FileStatus object to the list.
	 *
	 * @param FileStatus $fileStatus
	 *
	 * @return FileStatusList
	 */
	public function add( FileStatus $fileStatus ): self
	{
		$this->fileStatuses[] = $fileStatus;
		
		return $this;
	}
	
	/**
	 * Get a FileStatus object from the list.
	 *
	 * @param int $index
	 *
	 * @return null|FileStatus
	 */
	public function get( int $index ): ?FileStatus
	{
		return isset( $this->fileNames[ $index ] )
			? $this->fileStatuses[ $index ]
			: null;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function count()
	{
		return count( $this->fileStatuses );
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function current()
	{
		return $this->fileStatuses[ $this->current ];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function next()
	{
		$this->current ++;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function key()
	{
		return $this->current;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function valid()
	{
		return isset( $this->fileStatuses[ $this->current ] );
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rewind()
	{
		$this->current = 0;
	}
}