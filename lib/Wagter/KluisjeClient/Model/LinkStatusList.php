<?php

namespace Wagter\KluisjeClient\Model;

/**
 * Class LinkStatusList
 *
 * @package Wagter\KluisjeClient\Model
 *
 * @author Joris Wagter
 */
class LinkStatusList implements \Countable, \Iterator
{
	/**
	 * The array containing the LinkStatus objects.
	 *
	 * @var LinkStatus[]
	 */
	private $linkStatuses = [];
	
	/**
	 * The current index of the iterator
	 *
	 * @var int
	 */
	private $current = 0;
	
	/**
	 * LinkStatusList constructor.
	 *
	 * @param LinkStatus[] $linkStatuses
	 */
	public function __construct( array $linkStatuses = [] )
	{
		foreach ( $linkStatuses as $linkStatus ) {
			$this->add( $linkStatus );
		}
	}
    
    /**
     * Add a LinkStatus to the list.
     *
     * @param LinkStatus $linkStatus
     *
     * @return LinkStatusList
     */
	public function add( LinkStatus $linkStatus ): self
	{
		$this->linkStatuses[] = $linkStatus;
		
		return $this;
	}
	
	/**
	 * Get a LinkStatus object from the list.
	 *
	 * @param int $index
	 *
	 * @return null|LinkStatus
	 */
	public function get( int $index ): ?LinkStatus
	{
		return isset( $this->linkStatuses[ $index ] )
			? $this->linkStatuses[ $index ]
			: null;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function count()
	{
		return count( $this->linkStatuses );
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function current()
	{
		return $this->linkStatuses[ $this->current ];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function next()
	{
		$this->current ++;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function key()
	{
		return $this->current;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function valid()
	{
		return isset( $this->linkStatuses[ $this->current ] );
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rewind()
	{
		$this->current = 0;
	}
}