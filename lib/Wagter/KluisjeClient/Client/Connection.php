<?php

namespace Wagter\KluisjeClient\Client;

/**
 * Containing a Curl handle, methods to set options and get results
 *
 * Class Connection
 * @package Wagter\KluisjeClient\Client
 *
 * @author Joris Wagter
 */
class Connection
{
    /**
     * The Curl handle
     *
     * @var resource
     */
    private $curlHandle;
    
    /**
     * Connection constructor.
     *
     * @param string $url
     * @param string $userName
     * @param string $password
     */
    public function __construct( string $url, string $userName, string $password )
    {
        $this->curlHandle = curl_init( $url );
        
        $this
            ->setOption( CURLOPT_HEADER, 1 )
            ->setOption( CURLOPT_USERPWD, $userName . ":" . $password )
            ->setOption( CURLOPT_TIMEOUT, 30 )
            ->setOption( CURLOPT_RETURNTRANSFER, true )
        ;
    }
    
    /**
     * Set a Curl option
     * @see http://php.net/manual/en/function.curl-setopt.php
     *
     * @param int $option
     * @param $value
     *
     * @return Connection
     */
    public function setOption( int $option, $value ): Connection
    {
        curl_setopt( $this->curlHandle, $option, $value );
        
        return $this;
    }
    
    /**
     * Get the results of the Curl connection
     *
     * @return null|string
     */
    public function getResult(): ?string
    {
        $result = curl_exec( $this->curlHandle );
        $this->close();
    
        return $result !== false ? $result : null;
    }
    
    /**
     * Close the connection
     */
    public function close()
    {
        curl_close( $this->curlHandle );
    }
}