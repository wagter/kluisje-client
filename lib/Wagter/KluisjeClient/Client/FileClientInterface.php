<?php

namespace Wagter\KluisjeClient\Client;

use Wagter\KluisjeClient\Model\FileStatus;
use Wagter\KluisjeClient\Model\FileStatusList;

/**
 * To implement a client for LinkStatus objects of the Kluisje API
 *
 * Interface FileClientInterface
 * @package Wagter\KluisjeClient\Client
 */
interface FileClientInterface
{
    /**
     * Get the index of the files in the Kluisje API files directory
     *
     * @return FileStatusList
     */
    public function index(): FileStatusList;
    
    /**
     * Get the status of a file in the Kluisje API files directory
     *
     * @param string $fileName
     *
     * @return FileStatus
     */
    public function status( string $fileName ): FileStatus;
    
    /**
     * Upload a new file to the Kluisje API files directory
     *
     * @param array $fileInfo
     * @param string|null $fileName
     *
     * @return FileStatus
     */
    public function upload( array $fileInfo, string $fileName = null ): FileStatus;
    
    /**
     * Delete a file from the Kluisje API files directory
     *
     * @param string $fileName
     *
     * @return FileStatus
     */
    public function delete( string $fileName ): FileStatus;
}