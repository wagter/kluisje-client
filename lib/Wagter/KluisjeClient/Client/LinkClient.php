<?php

namespace Wagter\KluisjeClient\Client;

use Wagter\KluisjeClient\Model\LinkStatus;
use Wagter\KluisjeClient\Model\LinkStatusList;
use Wagter\KluisjeClient\Normalizer\Denormalizer\LinkStatusDenormalizerInterface;

/**
 * Class LinkClient
 * @package Wagter\KluisjeClient\Client
 */
class LinkClient extends AbstractClient implements LinkClientInterface
{
    /**
     * @var LinkStatusDenormalizerInterface
     */
    private $denormalizer;
    
    /**
     * LinkClient constructor.
     *
     * @param LinkStatusDenormalizerInterface $denormalizer
     * @param string $baseUrl
     * @param string $userName
     * @param string $password
     * @param string $routePrefix
     */
    public function __construct(
        LinkStatusDenormalizerInterface $denormalizer,
        string $baseUrl,
        string $userName,
        string $password,
        string $routePrefix = 'link'
    ) {
        parent::__construct( $baseUrl, $userName, $password, $routePrefix );
        
        $this->denormalizer = $denormalizer;
    }
    
    /**
     * {@inheritdoc}
     */
    public function index(): LinkStatusList
    {
        $conn   = $this->getConnection( sprintf( '%s/index', $this->getBaseUrl() ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeListFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function generate( string $fileName ): LinkStatus
    {
        $conn   = $this->getConnection( sprintf( '%s/generate/%s', $this->getBaseUrl(), $fileName ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function status( string $hash ): LinkStatus
    {
        $conn   = $this->getConnection( sprintf( '%s/status/%s', $this->getBaseUrl(), $hash ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function invalidate( string $hash ): LinkStatus
    {
        $conn   = $this->getConnection( sprintf( '%s/invalidate/%s', $this->getBaseUrl(), $hash ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete( string $hash ): LinkStatus
    {
        $conn   = $this->getConnection( sprintf( '%s/delete/%s', $this->getBaseUrl(),  $hash ) );
        $result = $conn->getResult();
    
        return $this->denormalizer->denormalizeFromJson( $result );
    }
}