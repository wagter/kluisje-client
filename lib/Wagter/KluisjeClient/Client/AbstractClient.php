<?php

namespace Wagter\KluisjeClient\Client;

use Wagter\KluisjeClient\Normalizer\Denormalizer\LinkStatusDenormalizerInterface;
use Wagter\KluisjeClient\Model\LinkStatus;

/**
 * Class AbstractKluisjeClient
 * @package Wagter\KluisjeClient\Client
 */
abstract class AbstractClient
{
    /**
     * The URL to the domain hosting the Kluisje API.
     *
     * @var string
     */
    private $baseUrl;
    
    /**
     * The user name for authenticating with the Kluisje API
     *
     * @var string
     */
    private $userName;
    
    /**
     * The password for authenticating with the Kluisje API
     *
     * @var string
     */
    private $password;
    
    /**
     * The route prefix for the controller of the Kluisje API
     *
     * @var string
     */
    private $routePrefix;
    
    /**
     * DownloadVaultClient constructor.
     *
     * @param string $baseUrl
     * @param string $userName
     * @param string $password
     * @param string $routePrefix
     */
    public function __construct(
        string $baseUrl,
        string $userName,
        string $password,
        string $routePrefix = ''
    ) {
        $this->baseUrl     = $baseUrl;
        $this->userName    = $userName;
        $this->password    = $password;
        $this->routePrefix = $routePrefix;
    }
    
    /**
     * Get a new connection for a URL
     *
     * @param string $url
     *
     * @return Connection
     */
    protected function getConnection( string $url ): Connection
    {
        return new Connection( $url, $this->userName, $this->password );
    }
    
    /**
     * Get the base URL of the Kluisje API
     *
     * @return string
     */
    protected function getBaseUrl(): string
    {
        $url = $this->baseUrl;
        
        if ( $this->routePrefix !== '' ) {
            $url .= '/' . $this->getRoutePrefix();
        }
        
        return $url;
    }
    
    /**
     * Get the route prefix for the controller of the Kluisje API
     *
     * @return string
     */
    protected function getRoutePrefix(): string
    {
        return $this->routePrefix;
    }
}