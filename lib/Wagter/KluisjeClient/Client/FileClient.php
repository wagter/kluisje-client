<?php

namespace Wagter\KluisjeClient\Client;


use Wagter\KluisjeClient\Model\FileStatus;
use Wagter\KluisjeClient\Model\FileStatusList;
use Wagter\KluisjeClient\Normalizer\Denormalizer\FileStatusDenormalizerInterface;

class FileClient extends AbstractClient implements FileClientInterface
{
    /**
     * @var FileStatusDenormalizerInterface
     */
    private $denormalizer;
    
    /**
     * FileClient constructor.
     *
     * @param FileStatusDenormalizerInterface $denormalizer
     * @param string $baseUrl
     * @param string $userName
     * @param string $password
     * @param string $routePrefix
     */
    public function __construct(
        FileStatusDenormalizerInterface $denormalizer,
        string $baseUrl,
        string $userName,
        string $password,
        string $routePrefix = 'file'
    ) {
        parent::__construct( $baseUrl, $userName, $password, $routePrefix );
        
        $this->denormalizer = $denormalizer;
    }
    
    /**
     * {@inheritdoc}
     */
    public function index(): FileStatusList
    {
        $conn   = $this->getConnection( sprintf( '%s/index', $this->getBaseUrl() ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeListFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function status( string $fileName ): FileStatus
    {
        $conn   = $this->getConnection( sprintf( '%s/status/%s', $this->getBaseUrl(), $fileName ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function upload( array $fileInfo, string $fileName = null ): FileStatus
    {
        if ( is_string( $fileName ) ) {
            $fileInfo['name'] = $fileName;
        }
        
        $conn = $this->getConnection( sprintf( '%s/upload', $this->getBaseUrl() ) );
        
        $file = sprintf('@%s;filename=%s;type=%s', $fileInfo['tmp_name'], $fileInfo['name'], $fileInfo['type'] );
        
        $conn
            ->setOption( CURLOPT_POST, 1 )
            ->setOption( CURLOPT_POSTFIELDS, [ 'file' => $file ] )
        ;
        
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeFromJson( $result );
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete( string $fileName ): FileStatus
    {
        $conn   = $this->getConnection( sprintf( '%s/delete/%s', $this->getBaseUrl(), $fileName ) );
        $result = $conn->getResult();
        
        return $this->denormalizer->denormalizeFromJson( $result );
    }
}