<?php

namespace Wagter\KluisjeClient\Client;

use Wagter\KluisjeClient\Model\LinkStatus;
use Wagter\KluisjeClient\Model\LinkStatusList;

/**
 * To implement a client for LinkStatus objects of the Kluisje API
 *
 * Interface LinkClientInterface
 * @package Wagter\KluisjeClient\Client
 *
 * @author Joris Wagter
 */
interface LinkClientInterface
{
    /**
     * Get the index of download links in the Kluisje API database
     *
     * @return LinkStatusList
     */
    public function index(): LinkStatusList;
    
    /**
     * Generate a download link for a file on the server of the Kluisje API
     *
     * @param string $fileName
     *
     * @return LinkStatus
     */
    public function generate( string $fileName ): LinkStatus;
    
    /**
     * Get the status of a download link in the Kluisje API database
     *
     * @param string $hash
     *
     * @return LinkStatus
     */
    public function status( string $hash ): LinkStatus;
    
    /**
     * Invalidate a download link in the Kluisje API database
     *
     * @param string $hash
     *
     * @return LinkStatus
     */
    public function invalidate( string $hash ): LinkStatus;
    
    /**
     * Delete a download link from the Kluisje API database
     *
     * @param string $hash
     *
     * @return LinkStatus
     */
    public function delete( string $hash ): LinkStatus;
}